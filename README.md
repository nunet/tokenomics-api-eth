# Tokenomics

# Steps To Run

## Build
``` 
	docker build -t tokenomics .
```
## Start
``` 
    docker run --name tokenomics -it tokenomics
```


# Test

## In a new terminal
``` 
docker exec -it tokenomics bash
```
``` 
python3 test_tokenomics.py
```