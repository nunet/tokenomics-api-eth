FROM ubuntu:18.04

RUN apt-get update && apt-get install -y --no-install-recommends \
        sudo \
        git \
        build-essential \
        python-setuptools \
        libpq-dev\
        pgloader\
        cmake \
        wget \
        curl \
        libsm6 \
        libxext6 \
        libxrender-dev \
        vim

RUN apt install libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev -y
RUN apt install wget -y
RUN wget https://www.python.org/ftp/python/3.7.5/Python-3.7.5.tgz --no-check-certificate

RUN tar -xzvf Python-3.7.5.tgz
WORKDIR Python-3.7.5
RUN ./configure --enable-optimizations

RUN make

RUN apt install zlib1g-dev libsqlite3-dev wget libbz2-dev -y

RUN make altinstall

RUN update-alternatives --install /usr/bin/python3 python3 /usr/local/bin/python3.7 1



RUN pip3.7 install web3

WORKDIR /TOKENOMICS-API
COPY requirements.txt /TOKENOMICS-API/

RUN pip3.7 install -r /TOKENOMICS-API/requirements.txt


ARG CONFIG

ENV CONFIG=${CONFIG}

ARG rpc_address

ENV rpc_address=${rpc_address}

ENV LS_SERVICE_NAME=tokenomics
ENV LS_ACCESS_TOKEN=ej4S2fSomE5V7uP/LYJLw9frBRnBRqHVoQiRYSCwoSld1MncIAHByOSkc8jcKiSAbhVEy2zI0Emjw38vVF8c87vFd6Q1wMCnI/DD4/Bi
ENV OTEL_PYTHON_TRACER_PROVIDER=sdk_tracer_provider
ENV jaeger_address=testserver.nunet.io
RUN pip3.7 install opentelemetry-launcher
RUN opentelemetry-bootstrap -a install



RUN pip3.7 install opentelemetry-exporter-jaeger==1.2.0
RUN pip3.7 install opentelemetry-api==1.3.0
RUN pip3.7 install opentelemetry-sdk==1.3.0
COPY . /TOKENOMICS-API

RUN sh buildproto.sh


CMD ["python3", "tokenomics.py"]

